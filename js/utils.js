const remote = require("@electron/remote");
const {v4: uuidv4} = require("uuid");

class UtilToolService{
    static getMainWindow(){
        return remote.getCurrentWindow()
    }
    static createUUID(){
        return uuidv4()
    }
}

module.exports = {
    UtilToolService
}
