const log4js = require('log4js');
const fs = require('fs');
const path = require('path');

log4js.configure({
    appenders: {
        out: {type: 'console'},     //控制台输出
        dev: {
            type: 'dateFile',
            filename: 'runtime/logs/dev.log',
            pattern: '-yyyy-MM-dd',
            alwaysIncludePattern: false,
        },
        ipc: {
            type: 'dateFile',
            filename: 'runtime/logs/ipc.log',
            pattern: '-yyyy-MM-dd',
            alwaysIncludePattern: false,
        },
        im: {
            type: 'dateFile',
            filename: 'runtime/logs/im.log',
            pattern: '-yyyy-MM-dd',
            alwaysIncludePattern: false,
        },
    },
    categories: {
        default: {appenders: ['out'], level: 'ALL'},
        logFile: {appenders: ['out', 'dev'], level: 'ALL'},
        ipcLog: {appenders: ['out', 'ipc'], level: 'ALL'},
        ipcLogFile: {appenders: ['ipc'], level: 'ALL'},
        imLogFile: {appenders: ['im'], level: 'ALL'},
    },
    replaceConsole: true,       //替换console.log
});


module.exports = {
    outLog: log4js.getLogger('default'),
    fileLog: log4js.getLogger('logFile'),
    ipcLog: log4js.getLogger('ipcLog'),
    ipcLogFile: log4js.getLogger('ipcLogFile'),
    imLogFile: log4js.getLogger('imLogFile'),
};
