const { app, BrowserWindow,ipcMain } = require('electron')
const path = require('path');
const remote = require('@electron/remote/main');

const createWindow = () => {
    const win = new BrowserWindow({
        width: 1100,
        height: 600,
        webPreferences:{
            nodeIntegration: true,
            contextIsolation: false,
            //关闭沙盒
            sandbox: false,
            preload: path.join(__dirname, './preload.js'),
        }

    })
    remote.enable(win.webContents);
    win.loadFile('index.html')
    win.webContents.openDevTools({mode: 'undocked'});
}

app.whenReady().then(() => {

    remote.initialize();

    ipcMain.on('api:screen:getPrimaryDisplayAsync', (event, arg) => {
        try {
            const {screen} = require('electron');
            event.returnValue = screen.getPrimaryDisplay();
        } catch (e) {

        }
    });

    createWindow()

    app.on('activate', () => {
        if (BrowserWindow.getAllWindows().length === 0) createWindow()
    })
})


app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') app.quit()
})
