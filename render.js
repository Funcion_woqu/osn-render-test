const {ObsQuickTools}= require('./js/obs-quick-tools')


function sleepfunc(time) {
    return new Promise(resolve => {
        setTimeout(() => {
            resolve(true);
        }, time);
    });
}

async function _init_func() {
    var previewContainer = document.getElementById('old-display-el');
    const {width, height, x, y} = previewContainer.getBoundingClientRect();
    //
    console.log({width, height, x, y});
    ObsQuickTools.createObsHandler(true);
    await sleepfunc(0.5);
    const result = await ObsQuickTools.startObsInstance({
      bounds: {width, height, x, y},
      type: 'screen',
    });

    console.log(`ObsEasy.setupPreview result is ${JSON.stringify(result)}`);
    previewContainer.style.height = `${result.height}px`;
}
window.addEventListener('DOMContentLoaded', () => {
    // ObsQuickTools.createObsHandler(true)
    // ObsQuickTools.startObsInstance()
    _init_func()
})
