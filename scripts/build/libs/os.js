const fs = require('fs')
const path = require('path')


const copyDirFn = (originDirPath, toDirPath) => {
  return new Promise((resolve, reject) => {

    // console.log(JSON.stringify({originDirPath, toDirPath}))
    // 复制文件
    fs.cp(originDirPath, toDirPath, { recursive: true }, (err) => {
      if (err) {
        console.error(`复制文件夹失败${originDirPath}===>${toDirPath}`)
        reject(err)
      }
      // console.error(`复制依赖成功${originDirPath}===>${toDirPath}`)
      resolve(true)
    })
  })
}

/**
 * 递归创建目录 异步方法
 * @param dirname
 * @param callback
 */
function mkdirs(dirname, callback) {
  fs.exists(dirname, function (exists) {
    if (exists) {
      callback()
    } else {
      // console.log(path.dirname(dirname));
      mkdirs(path.dirname(dirname), function () {
        fs.mkdir(dirname, callback)
        // console.log('在' + path.dirname(dirname) + '目录创建好' + dirname + '目录');
      })
    }
  })
}

/**
 * 递归创建目录 同步方法
 * @param dirname
 * @return {boolean}
 */
function mkdirsSync(dirname) {
  if (fs.existsSync(dirname)) {
    return true
  }
  if (mkdirsSync(path.dirname(dirname))) {
    fs.mkdirSync(dirname)
    return true
  }
}

module.exports = {
  copyDirFn,
  mkdirsSync,
  mkdirs,
}
